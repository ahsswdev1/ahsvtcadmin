package org.ahs.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ahs.dao.LocationDao;
import org.ahs.model.Location;
import org.ahs.dao.AgentDao;
import org.ahs.model.Agent;;


public class AgentController extends HttpServlet {
    private static final long serialVersionUID = 1L;
 //do we need INSERT_OR_EDIT and INSERT_OR_EDIT_ANS?
    private static String INSERT_OR_EDIT = "/agent.jsp";
    private static String LIST_LOCATION = "/listLocation.jsp";
    private static String LIST_AGENT = "/listAgent.jsp";
    private static String INSERT_OR_EDIT_ANS = "/agent.jsp";
    private LocationDao locationDao;
    private AgentDao agentDao;
    public AgentController() {
        super();
        locationDao = new LocationDao();
        agentDao= new AgentDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward="";
        String action = request.getParameter("action");
        int locationId =0;
        System.out.println("action=" + action);
        if (action.equalsIgnoreCase("delete")){ 
        	int agentId = Integer.parseInt(request.getParameter("agentId"));
            agentDao.deleteAgent(agentId);
            forward = LIST_LOCATION;
            request.setAttribute("locations", locationDao.getAllLocations()); 
            
        } else if (action.equalsIgnoreCase("edit")){
            forward = INSERT_OR_EDIT_ANS;
            System.out.println("test4");
            int agentId = Integer.parseInt(request.getParameter("agentId"));
            Agent agent = agentDao.getAgentById(agentId);
            request.setAttribute("agent", agent);
            
        } else if (action.equalsIgnoreCase("listAgent")){
            forward = LIST_AGENT;
            System.out.println("test3");
            locationId= Integer.parseInt(request.getParameter("locationId"));
            //request.setAttribute("agents", agentDao.getAllAgents());
            
        }  else if (action.equalsIgnoreCase("editAgents")){
            forward = LIST_AGENT;
            System.out.println("test1");
            locationId= Integer.parseInt(request.getParameter("locationId"));
            request.setAttribute("agents", agentDao.getAllAgents(locationId));         
            
        }
        
        else {
            forward = INSERT_OR_EDIT_ANS;   
        }

        System.out.println("test2");
        
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Agent agent = new Agent();
        System.out.println("test5 post");
        agent.setAgentFirst_Name(request.getParameter("agentFirst_Name"));
        agent.setAgentLast_Name(request.getParameter("agentLast_Name"));
        agent.setAgentEmail(request.getParameter("agentEmail"));
        agent.setAgentPhone_Number(request.getParameter("agentPhone_Number"));
        agent.setAgentBio(request.getParameter("agentBio"));
        agent.setAgentImage(request.getParameter("agentImage"));
        String locInput = request.getParameter("agentLocationId");
        agent.setAgentVideo(request.getParameter("agentVideo"));
        try{
	        agent.setAgentLocationId(Integer.parseInt(locInput));      
	        String id = request.getParameter("agentId");
	        System.out.println("This is ID #"+ id);
	        if(id == null || id.isEmpty())
	        {
	        	agentDao.addAgent(agent);
	        	System.out.println("test66");
	        }
	        else
	        {
	            agent.setAgentId(Integer.parseInt(id));
	            agentDao.updateAgent(agent);
	        	System.out.println("id is "+ id);
	        }
        }
        catch (NumberFormatException ex){
        	System.out.println("NumberFormat Exception:\n"+ex);
        	System.out.println("We got: \""+locInput+"\"");
        }
        RequestDispatcher view = request.getRequestDispatcher(LIST_AGENT);
        request.setAttribute("agents", agentDao.getAllAgents(Integer.parseInt(locInput)));
        view.forward(request, response);
        
    }
}