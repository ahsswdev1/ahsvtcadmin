package org.ahs.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ahs.dao.LocationDao;
import org.ahs.model.Location;
import org.ahs.dao.AgentDao;
import org.ahs.model.Agent;;


public class LocationController extends HttpServlet 
{
    private static final long serialVersionUID = 1L;
    private static String INSERT_OR_EDIT = "/location.jsp";
    private static String LIST_LOCATION = "/listLocation.jsp";
    private static String LIST_AGENT = "/listAgent.jsp";
    private static String INSERT_OR_EDIT_ANS = "/agent.jsp";
    private LocationDao locationDao;
    private AgentDao agentDao;
    public LocationController() {
        super();
        locationDao = new LocationDao();
        agentDao= new AgentDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward="";
        String action = request.getParameter("action");
        
        System.out.println("action=" + action);
        
        if (action.equalsIgnoreCase("delete")){
        	int locationId = Integer.parseInt(request.getParameter("locationId"));
            locationDao.deleteLocation(locationId);
            forward = LIST_LOCATION;
            request.setAttribute("locations", locationDao.getAllLocations());    
        } else if (action.equalsIgnoreCase("edit")){
            forward = INSERT_OR_EDIT;
            System.out.println("test4");
            int locationId = Integer.parseInt(request.getParameter("locationId"));
            Location location = locationDao.getLocationById(locationId);
            request.setAttribute("location", location);
        } else if (action.equalsIgnoreCase("listLocation")){
            forward = LIST_LOCATION;
            System.out.println("test3");
            request.setAttribute("locations", locationDao.getAllLocations());
        }  else if (action.equalsIgnoreCase("editAgents")){
            forward = LIST_AGENT;
            System.out.println("test1");
            int locationid = Integer.parseInt(request.getParameter("locationId"));
            request.setAttribute("agents", agentDao.getAllAgents(locationid));
            request.setAttribute("locationId", locationid);
            
        }
        
        else {
            forward = INSERT_OR_EDIT;
        }

        System.out.println("test2");
        
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Location location = new Location();
        System.out.println("test5 post");
        location.setLocationCity(request.getParameter("locationCity"));
        location.setLocationState(request.getParameter("locationState"));
        location.setLocationCountry(request.getParameter("locationCountry"));
        String id = request.getParameter("locationId");
        
        if(id == null || id.isEmpty())
        {
        	locationDao.addLocation(location);
        }
        else
        {
            location.setLocationId(Integer.parseInt(id));
            locationDao.updateLocation(location);
        }
        RequestDispatcher view = request.getRequestDispatcher(LIST_LOCATION);
        request.setAttribute("locations", locationDao.getAllLocations());
        view.forward(request, response);
    }
}