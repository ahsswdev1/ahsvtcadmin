package org.ahs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.ahs.model.Location;
import org.ahs.util.DbUtil;

public class LocationDao {

    private Connection connection;
 
    public LocationDao() {
        connection = DbUtil.getConnection();
    }

    public void addLocation(Location location) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("insert into location(city, state, country ) values (?, ?, ?)");
            // Parameters start with 1
            preparedStatement.setString(1, location.getLocationCity());
            preparedStatement.setString(2, location.getLocationState().trim());
            preparedStatement.setString(3, location.getLocationCountry());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteLocation(int locationId) {
        try {
       	PreparedStatement preparedStatement = connection
                    .prepareStatement("delete from location where id=?");
            // Parameters start with 1
            preparedStatement.setInt(1, locationId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateLocation(Location location) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("update location set state = ?, city = ?, country = ? " +
                            "where id=?");
            // Parameters start with 1
            preparedStatement.setString(1, (location.getLocationState().trim()));
            preparedStatement.setString(2, location.getLocationCity());
            preparedStatement.setString(3, location.getLocationCountry());
            preparedStatement.setInt(4, location.getLocationId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Location> getAllLocations() {
        List<Location> locations = new ArrayList<Location>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select * from location");
            while (rs.next()) {
                Location location = new Location();
                location.setLocationId(rs.getInt("id"));
                location.setLocationCity(rs.getString("city"));
                location.setLocationState(rs.getString("state"));
                location.setLocationCountry(rs.getString("country"));
                locations.add(location);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return locations;
    }

    public Location getLocationById(int locationId) {
        Location location = new Location();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("select * from location where id=?");
            preparedStatement.setInt(1, locationId);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                location.setLocationId(rs.getInt("id"));
                location.setLocationState(rs.getString("state"));
                location.setLocationCity(rs.getString("city"));
                location.setLocationCountry(rs.getString("country"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return location;
    }
}