
package org.ahs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.ahs.model.Agent;
import org.ahs.util.DbUtil;

public class AgentDao {

    private Connection connection;
 
    public AgentDao() {
        connection = DbUtil.getConnection();
    }

    public void addAgent(Agent agent) {
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("insert into agents(first_name, last_name, phone_number, email, locationid, bio, image, video) values (?, ?, ?, ?, ?, ?, ?, ?)");
            // Parameters start with 1
            preparedStatement.setString(1, agent.getAgentFirst_Name());
            preparedStatement.setString(2, agent.getAgentLast_Name());
            preparedStatement.setString(3, agent.getAgentPhone_Number());
            preparedStatement.setString(4, agent.getAgentEmail());
            preparedStatement.setInt(5, agent.getAgentLocationId());
            preparedStatement.setString(6, agent.getAgentBio());
            preparedStatement.setString(7, agent.getAgentImage());
            preparedStatement.setString(8, agent.getAgentVideo());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAgent(int agentId) {
        try {
       	PreparedStatement preparedStatement = connection
                    .prepareStatement("delete from agents where id=?");
            // Parameters start with 1
            preparedStatement.setInt(1, agentId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateAgent(Agent agent) { 
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("update agents set first_name = ?, last_name=?, phone_number=?, email=?, bio=?, image=?, video=? " +
                            "where id= ? ");
            // Parameters start with 1
            preparedStatement.setString(1, agent.getAgentFirst_Name());
            preparedStatement.setString(2, agent.getAgentLast_Name());
            preparedStatement.setString(3, agent.getAgentPhone_Number());
            preparedStatement.setString(4, agent.getAgentEmail());
            preparedStatement.setString(5, agent.getAgentBio());
            preparedStatement.setString(6, agent.getAgentImage());
            preparedStatement.setString(7, agent.getAgentVideo());
            preparedStatement.setInt(8, agent.getAgentId());
            System.out.println("phone number is " + agent.getAgentPhone_Number());
            System.out.println(agent.getAgentFirst_Name());
            System.out.println(agent.getAgentLast_Name());
            System.out.println(agent.getAgentEmail());
            System.out.println("Update: "+preparedStatement.toString());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Agent> getAllAgents(int locationId) {
        List<Agent> agents = new ArrayList<Agent>();
        try {
        	PreparedStatement preparedStatement = connection.
                    prepareStatement("select * from agents where locationid=?");
            preparedStatement.setInt(1, locationId);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Agent agent = new Agent();
                agent.setAgentId(rs.getInt("id"));
                agent.setAgentFirst_Name(rs.getString("first_name"));
                agent.setAgentLast_Name(rs.getString("last_name"));
                agent.setAgentPhone_Number(rs.getString("phone_number"));
                agent.setAgentEmail(rs.getString("email"));
                agent.setAgentBio(rs.getString("bio"));
                agent.setAgentImage(rs.getString("image"));
                agent.setAgentLocationId(rs.getInt("locationid"));
                agent.setAgentVideo(rs.getString("video"));
                agents.add(agent);
            }
            System.out.println("get all agents"+preparedStatement.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return agents;
    }

    public Agent getAgentById(int agentId) {
        Agent agent = new Agent();
        try {
            PreparedStatement preparedStatement = connection.
                    prepareStatement("select * from agents where id=?");
            preparedStatement.setInt(1, agentId);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                agent.setAgentId(rs.getInt("id"));
                agent.setAgentFirst_Name(rs.getString("first_name"));
                agent.setAgentLast_Name(rs.getString("last_name"));
                agent.setAgentPhone_Number(rs.getString("phone_number"));
                agent.setAgentEmail(rs.getString("email"));
                agent.setAgentBio(rs.getString("bio"));
                agent.setAgentImage(rs.getString("image"));
                agent.setAgentLocationId(rs.getInt("locationid"));
                agent.setAgentVideo(rs.getString("video"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return agent;
    }
}