package org.ahs.model;

public class Agent {

	private int agentId;
	private String agentFirst_Name;
	private String agentLast_Name;
	private String agentPhone_Number;
	private String agentEmail;
    private int agentLocationId;
    private String agentBio;
    private String agentImage;
    private String agentVideo;
	
    public int getAgentId() {
        return agentId;
    }
    public void setAgentId(int agentId) {
        this.agentId = agentId;
    }
    public String getAgentFirst_Name() {
        return agentFirst_Name;
    }
    public void setAgentFirst_Name(String agentFirst_Name) {
        this.agentFirst_Name = agentFirst_Name;
    }
    public String getAgentLast_Name() {
        return agentLast_Name;
    }
    public void setAgentLast_Name(String agentLast_Name) {
        this.agentLast_Name = agentLast_Name;
    }
    public String getAgentPhone_Number() {
        return agentPhone_Number;
    }
    public void setAgentPhone_Number(String agentPhone_Number) {
        this.agentPhone_Number = agentPhone_Number;
    }
    public String getAgentEmail() {
        return agentEmail;
    }
    public void setAgentEmail(String agentEmail) {
        this.agentEmail = agentEmail;
    }
    public int getAgentLocationId() {
        return agentLocationId;
    }
    public void setAgentLocationId(int agentLocationId) {
        this.agentLocationId = agentLocationId;
    }
    public String getAgentBio() {
        return agentBio;
    }
    public void setAgentBio(String agentBio) {
        this.agentBio = agentBio;
    }
    public String getAgentImage() {
        return agentImage;
    }
    public void setAgentImage(String agentImage) {
        this.agentImage = agentImage;
    }
    public String getAgentVideo() {
    	return agentVideo;
    }
    public void setAgentVideo(String agentVideo) {
    	this.agentVideo = agentVideo;
    }
    
    
    
}
