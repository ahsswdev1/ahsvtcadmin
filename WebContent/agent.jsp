<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<link type="text/css"
    href="css/ui-lightness/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
<title>Add new agent</title>
</head>
<body>
    <div class="container">
        <div class="jumbotron">
    <form method="POST" action='AgentController' name="frmAddAgent">
       <h1> Add Agent / Update Agent </h1> 
       <p> Agent First Name : <input type="text" name="agentFirst_Name" 
            value="<c:out value="${agent.agentFirst_Name}" />" /> <br /> 
            
       <p> Agent Last Name : <input type="text" name="agentLast_Name" 
            value="<c:out value="${agent.agentLast_Name}" />" /> <br /> 
            
       <p> Agent Phone Number : <input type="text" name="agentPhone_Number" 
            value="<c:out value="${agent.agentPhone_Number}" />" /> <br /> 

	<p>	Agent Email Address: <input type="email" name="agentEmail" required placeholder="Enter a valid email address" 
			value="<c:out value="${agent.agentEmail}" />" /> <br />
       
      <p>  Agent Bio : <input type="text" name="agentBio" 
            value="<c:out value="${agent.agentBio}" />" /> <br />
            
       <p> Agent Image URL :<input type="text" name="agentImage" 
            value="<c:out value="${agent.agentImage}" />" /> <br />  
        
       <p> Agent Location Id : <input type="text" name="agentLocationId" 
            value="<c:out value="${agent.agentLocationId}" />" /> <br />
          
       <p> Video URL : <input type="text" name="agentVideo"
        	value="<c:out value="${agent.agentVideo}" />" /> <br />
        	   
        <input type="hidden" name = "agentId" value="<c:out value="${agent.agentId}" />" />
		<br /> <input type="submit" value="Submit" />
	</form>		
        </div>
    </div>

    <script src="js/jquery-1.11.3.js"></script>
    <script src="js/bootstrap.js"></script>
</body>
</html>