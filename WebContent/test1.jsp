<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <table class="table">
    <thead>
      <tr>
         <th>ID</th>
         <th>Location State</th>
         <th>Location City</th>
         <th>Update Info</th>
         <th colspan=3>Action</th>
      </tr>
    </thead>
    <tbody>
            <c:forEach items="${locations}" var="location">
                <tr>
                    <td><c:out value="${location.locationId}" /></td>
                    <td><c:out value="${location.locationState}" /></td>
                    <td><c:out value="${location.locationCity}" /></td>
                    <td><a href="LocationController?action=edit&locationId=<c:out value="${location.locationId}"/>">Update</a></td>
                    <td><a href="LocationController?action=delete&locationId=<c:out value="${location.locationId}"/>">Delete</a></td>
                    <td><a href="LocationController?action=editagents&locationId=<c:out value="${location.locationId}"/>">Edit Agents</a></td>
                </tr>
            </c:forEach>
     </tbody>
  </table>
</div>

</body>
</html>

