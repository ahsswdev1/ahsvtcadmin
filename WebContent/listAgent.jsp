<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>The Agents</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<h1>This is a list of all the Agents in a given location.</h1>
	<p></p>
    <div class="container">
    <table class="table"  style="overflow:hidden; width:50px;">
        <thead>
            <tr>
                <th>ID</th>
                <th>Agent First Name</th>
                <th>Agent Last Name</th>
                <th>Phone Number</th>
                <th>Email</th>
                <th>Image</th>
                <th>Bio</th>
                <th>Location Id</th>
                <th>Video URL</th>
                <th>Picture</th>
                
                <th colspan=3>Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${agents}" var="agent">
                <tr>
                    <td><c:out value="${agent.agentId}" /></td>
                    <td><c:out value="${agent.agentFirst_Name}" /></td>
                    <td><c:out value="${agent.agentLast_Name}" /></td>
                    <td><c:out value="${agent.agentPhone_Number}" /></td>
                    <td><c:out value="${agent.agentEmail}" /></td>
                    <td style="overflow:hidden; width:15px;"><c:out value="${agent.agentImage}" /></td>
                    <td><c:out value="${agent.agentBio}" /></td>
                    <td><c:out value="${agent.agentLocationId}" /></td>
                    <td style="overflow:hidden; width:15px;"><c:out value="${agent.agentVideo}" /></td>
                    <td><a href="${agent.agentImage}">
  						<img src=<c:out value="${agent.agentImage}"/> alt="HTML tutorial" style="width:42px;height:42px;border:0;"></a></td>
                    <td><a href="AgentController?action=edit&agentId=<c:out value="${agent.agentId}"/>">Update</a></td>
                    <td><a href="AgentController?action=delete&agentId=<c:out value="${agent.agentId}"/>">Delete</a></td>

                </tr>     
            </c:forEach>  
        </tbody>  
    </table>
    </div>
    
    <p><a href="AgentController?action=insert&locationId=<c:out value="${request.agentId}" />">Add Agent</a></p>
</body>
</html>
